#!/usr/bin/env node
import yargs from 'yargs';
import { createSimpleLogger } from 'simple-node-logger';
import { Context, PipeConfiguration } from '../types';
import { Pipe } from '..';

yargs
  .scriptName('queue-kit')
  .usage('$0 <command> [arguments]')
  .command<{
    configurationJson: string;
  }>(
    'pipe',
    'Pipe messages from one queue to another',
    (yargs) => {
      yargs.option('configuration-json', {
        type: 'string',
        required: true,
        describe: 'Configuration JSON',
      });
    },
    async (argv) => {
      const log = createSimpleLogger();
      const context: Context = {
        log,
      };

      try {
        const configuration = JSON.parse(
          argv.configurationJson
        ) as PipeConfiguration;
        const pipe = new Pipe(configuration, context);

        await pipe.initialize();
        await pipe.start();
      } catch (error) {
        log.fatal(error);
      }
    }
  )
  .help().argv;
