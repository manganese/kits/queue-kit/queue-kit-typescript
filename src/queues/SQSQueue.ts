import { Consumer as SQSConsumer } from 'sqs-consumer';
import { Producer as SQSProducer } from 'sqs-producer';
import { v4 as uuid } from 'uuid';
import { SQSQueueConfiguration, Queue } from '../types';
import { deserializeMessage, serializeMessage } from '../utilities';

export class SQSQueue<MessageType> implements Queue<MessageType> {
  static generateId(): string {
    return uuid();
  }

  private readonly sqsConsumer: SQSConsumer;
  private readonly sqsProducer: SQSProducer;

  constructor(private readonly configuration: SQSQueueConfiguration) {
    const sqsOptions = {
      queueUrl: configuration.url,
      region: configuration.region,
    };

    this.sqsConsumer = SQSConsumer.create({
      ...sqsOptions,
      handleMessage: async (sqsMessage) => {
        const message = deserializeMessage<MessageType>(
          Buffer.from(sqsMessage.Body)
        );

        await Promise.all(
          this.subscribeCallbacks.map((callback) => callback(message))
        );
      },
    });
    this.sqsProducer = SQSProducer.create({
      ...sqsOptions,
    });
  }

  public async initialize(): Promise<void> {
    await this.sqsProducer.queueSize();
  }

  private subscribeCallbacks: Array<(message: MessageType) => Promise<void>> =
    [];

  public async subscribe(callback: (message: MessageType) => Promise<void>) {
    this.subscribeCallbacks.push(callback);

    if (!this.sqsConsumer.isRunning) {
      this.sqsConsumer.start();
    }
  }

  public async publish(message: MessageType) {
    await this.sqsProducer.send({
      id: SQSQueue.generateId(),
      body: serializeMessage(message).toString(),
    });
  }
}
