import {
  AMQPChannel,
  AMQPClient,
  AMQPConsumer,
  AMQPQueue as AMQPQueueNative,
} from '@cloudamqp/amqp-client';
import { AMQPQueueConfiguration, Queue } from '../types';
import { deserializeMessage, serializeMessage } from '../utilities';

export class AMQPQueue<MessageType> implements Queue<MessageType> {
  private amqpClient: AMQPClient;
  private amqpChannel: AMQPChannel;
  private amqpQueue: AMQPQueueNative;
  private amqpConsumer: AMQPConsumer | null = null;

  constructor(private readonly configuration: AMQPQueueConfiguration) {
    this.amqpClient = new AMQPClient(configuration.url);
  }

  public async initialize() {
    const { queue } = this.configuration;
    const connection = await this.amqpClient.connect();

    this.amqpChannel = await connection.channel();

    await this.amqpChannel.queueDeclare(queue);

    this.amqpQueue = await this.amqpChannel.queue(queue);
  }

  public async publish(message: MessageType) {
    await this.amqpQueue.publish(serializeMessage(message));
  }

  public async subscribe(callback: (message: MessageType) => Promise<void>) {
    this.amqpConsumer = await this.amqpQueue.subscribe(
      { noAck: false },
      async (amqpMessage) => {
        const data = Buffer.from(amqpMessage.body);
        const message = deserializeMessage<MessageType>(data);

        await callback(message);

        amqpMessage.ack();
      }
    );
  }
}
