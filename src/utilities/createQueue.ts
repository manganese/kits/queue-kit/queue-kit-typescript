import { AMQPQueue, SQSQueue } from '../queues';
import {
  Queue,
  QueueConfiguration,
  QueueType,
  AMQPQueueConfiguration,
  SQSQueueConfiguration,
} from '../types';

export const createQueue = <MessageType>(
  queueConfiguration: QueueConfiguration
): Queue<MessageType> => {
  switch (queueConfiguration.type) {
    case QueueType.AMQP:
      const amqpQueueConfiguration =
        queueConfiguration as AMQPQueueConfiguration;

      return new AMQPQueue<MessageType>(amqpQueueConfiguration);
    case QueueType.SQS:
      const sqsQueueConfiguration = queueConfiguration as SQSQueueConfiguration;

      return new SQSQueue<MessageType>(sqsQueueConfiguration);
    default:
      throw new Error(`Unknown queue type: ${queueConfiguration}`);
  }
};
