export const serializeMessage = <MessageType>(message: MessageType): Buffer => {
  return Buffer.from(JSON.stringify(message));
};
