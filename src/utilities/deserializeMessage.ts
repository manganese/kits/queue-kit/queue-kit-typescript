export const deserializeMessage = <MessageType>(
  data: Buffer
): MessageType | null => {
  try {
    return JSON.parse(data.toString()) as MessageType;
  } catch (error) {
    return null;
  }
};
