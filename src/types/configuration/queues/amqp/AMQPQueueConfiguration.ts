import { QueueType } from '../../../queues';

export interface AMQPQueueConfiguration {
  type: QueueType.AMQP;
  url: string;
  queue: string;
}
