import { AMQPQueueConfiguration, SQSQueueConfiguration } from '.';

export type QueueConfiguration = AMQPQueueConfiguration | SQSQueueConfiguration;
