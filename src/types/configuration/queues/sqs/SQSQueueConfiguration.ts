import { QueueType } from '../../../queues';

export interface SQSQueueConfiguration {
  type: QueueType.SQS;
  url: string;
  region?: string;
}
