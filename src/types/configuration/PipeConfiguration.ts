import { QueueConfiguration } from './queues';

export interface PipeConfiguration {
  from: QueueConfiguration;
  to: QueueConfiguration;
}
