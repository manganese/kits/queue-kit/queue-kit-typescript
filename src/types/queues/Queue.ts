export interface Queue<MessageType = any> {
  initialize(): Promise<void>;
  publish(message: MessageType): Promise<void>;
  subscribe(callback: (message: MessageType) => Promise<void>): Promise<void>;
}
