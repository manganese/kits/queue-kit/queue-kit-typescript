export enum QueueType {
  AMQP = 'amqp',
  SQS = 'sqs',
}
