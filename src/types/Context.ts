import { Logger } from 'simple-node-logger';

export interface Context {
  log: Logger;
}
