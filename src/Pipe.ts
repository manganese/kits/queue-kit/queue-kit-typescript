import { Context, PipeConfiguration, Queue } from './types';
import { createQueue } from './utilities';

export class Pipe<MessageType = any> {
  private readonly fromQueue: Queue<MessageType>;
  private readonly toQueue: Queue<MessageType>;

  constructor(
    private readonly configuration: PipeConfiguration,
    private readonly context: Context
  ) {
    this.fromQueue = createQueue(configuration.from);
    this.toQueue = createQueue(configuration.to);
  }

  public async initialize(): Promise<void> {
    await Promise.all([this.fromQueue.initialize(), this.toQueue.initialize()]);
  }

  public async start(): Promise<void> {
    return this.fromQueue.subscribe(async (message) => {
      return this.toQueue.publish(message);
    });
  }
}
